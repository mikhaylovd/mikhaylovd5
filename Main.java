import java.util.HashSet;
import java.util.Scanner;

public class Main {
    static String deleteGrammar(String str){
        char[] grammar = {'!', ',','.','?','"',':',';'};
        for (char c : grammar){
            str = str.replace(Character.toString(c), "");
        }
        return str;
    }
    static boolean isLatinAlphabet(String str){
        String allLetters = "abcdefghijklmnopqrstuvwxyz";
        allLetters += allLetters.toUpperCase();
        for (String c : str.split("")){
            if (!allLetters.contains(c))return false;
        }
        return true;
    }
    static boolean isPalyndrom(String str){
        str = str.toLowerCase();
        for (int i = 0; i < str.length() / 2 + 1; i++){
            if (str.charAt(i) != str.charAt(str.length() - i - 1)) return false;
        }
        return true;
    }
    public static void main(String[] args) {
        String str;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        str = scanner.nextLine();
        String target = "object-oriented programming";
        String str2 = "";
        int i = 0;
        boolean flag = false;
        for (i = 0; i < str.length() - target.length(); i++){
            if (str.regionMatches(true, i, target, 0, target.length())){
                if (flag == true) {
                    str2 += "OOP";
                    i += target.length() - 1;
                    flag = false;
                }
                else {
                    str2 += Character.toString(str.charAt(i));
                    flag = true;
                }
            }
            else{
                str2 += Character.toString(str.charAt(i));
            }
        }
        str2 += (str.substring(i, str.length()));
        str = str2;
        System.out.println("Замена каждого второго вхождения:\n" + str);
        String minWord = str.split(" ")[0];
        int minUnique = minWord.length();
        for (String word : str.split(" ")){
            word = deleteGrammar(word);
            char[] chars = new char[word.length()];
            word.getChars(0, word.length(), chars, 0 );
            HashSet<Character> set = new HashSet<>();
            for (Character c : chars){
                set.add(c);
            }
            if (set.size() < minUnique){
                minUnique = set.size();
                minWord = word;
            }
        }
        System.out.println("Слово с минимальный кол-вом уникальных символов: " + minWord + " " + minUnique);
        int k = 0;
        for (String word : str.split(" ")){
            word = deleteGrammar(word);
            if (isLatinAlphabet(word)) k++;
        }
        System.out.println("Слов, состоящих только из букв латинского алфавита: " + k);
        System.out.println("Палиндромы: ");
        for (String word : str.split(" ")){
            word = deleteGrammar(word);
            if (isPalyndrom(word)) System.out.print(word + " ");
        }
    }
}
